﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin.IdentityServer.Models.ViewModels;
using Admin.IdentityServer.Service;
using Microsoft.AspNetCore.Mvc;

namespace Admin.IdentityServer.Controllers
{
    public class ConsentController : Controller
    {
        private readonly ConsentService _consentService;
        public ConsentController(ConsentService consentService)
        {
            _consentService = consentService;
        }

        public async Task<IActionResult> Index(string returnUrl)
        {
            var model = await _consentService.BuildConsentViewModel(returnUrl);
            if (model == null)
            {

            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Index(InputConsentViewModel viewModel)
        {
            var result = await _consentService.ProcessConsent(viewModel);
            if (result != null && result.IsRedirect)
            {
                return Redirect(result.RedirectUrl);
            }

            if (!string.IsNullOrEmpty(result.ValidationError))
            {
                ModelState.AddModelError(string.Empty, result.ValidationError);
            }
            return View(result.ViewModel);
        }
    }
}