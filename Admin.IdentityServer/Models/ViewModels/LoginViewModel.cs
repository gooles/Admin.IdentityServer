﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Admin.IdentityServer.Models.ViewModels
{
    public class LoginViewModel
    {

        [Required]
        [DataType(DataType.EmailAddress)]//内容检查是否为密码
        public string Email { get; set; }

        [Required]//必须的
        [DataType(DataType.Password)]//内容检查是否为密码
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
}
