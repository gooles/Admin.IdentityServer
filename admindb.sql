/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : admindb

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2019-07-31 15:37:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for apiclaims
-- ----------------------------
DROP TABLE IF EXISTS `apiclaims`;
CREATE TABLE `apiclaims` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `ApiResourceId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_ApiClaims_ApiResourceId` (`ApiResourceId`),
  CONSTRAINT `FK_ApiClaims_ApiResources_ApiResourceId` FOREIGN KEY (`ApiResourceId`) REFERENCES `apiresources` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of apiclaims
-- ----------------------------

-- ----------------------------
-- Table structure for apiproperties
-- ----------------------------
DROP TABLE IF EXISTS `apiproperties`;
CREATE TABLE `apiproperties` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Key` varchar(250) CHARACTER SET utf8mb4 NOT NULL,
  `Value` varchar(2000) CHARACTER SET utf8mb4 NOT NULL,
  `ApiResourceId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_ApiProperties_ApiResourceId` (`ApiResourceId`),
  CONSTRAINT `FK_ApiProperties_ApiResources_ApiResourceId` FOREIGN KEY (`ApiResourceId`) REFERENCES `apiresources` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of apiproperties
-- ----------------------------

-- ----------------------------
-- Table structure for apiresources
-- ----------------------------
DROP TABLE IF EXISTS `apiresources`;
CREATE TABLE `apiresources` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Enabled` tinyint(1) NOT NULL,
  `Name` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `DisplayName` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Description` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Created` datetime(6) NOT NULL,
  `Updated` datetime(6) DEFAULT NULL,
  `LastAccessed` datetime(6) DEFAULT NULL,
  `NonEditable` tinyint(1) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `IX_ApiResources_Name` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of apiresources
-- ----------------------------
INSERT INTO `apiresources` VALUES ('1', '1', 'api1', 'API Application', null, '2019-07-31 07:28:53.521247', null, null, '0');

-- ----------------------------
-- Table structure for apiscopeclaims
-- ----------------------------
DROP TABLE IF EXISTS `apiscopeclaims`;
CREATE TABLE `apiscopeclaims` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `ApiScopeId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_ApiScopeClaims_ApiScopeId` (`ApiScopeId`),
  CONSTRAINT `FK_ApiScopeClaims_ApiScopes_ApiScopeId` FOREIGN KEY (`ApiScopeId`) REFERENCES `apiscopes` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of apiscopeclaims
-- ----------------------------

-- ----------------------------
-- Table structure for apiscopes
-- ----------------------------
DROP TABLE IF EXISTS `apiscopes`;
CREATE TABLE `apiscopes` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `DisplayName` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Description` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Required` tinyint(1) NOT NULL,
  `Emphasize` tinyint(1) NOT NULL,
  `ShowInDiscoveryDocument` tinyint(1) NOT NULL,
  `ApiResourceId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `IX_ApiScopes_Name` (`Name`),
  KEY `IX_ApiScopes_ApiResourceId` (`ApiResourceId`),
  CONSTRAINT `FK_ApiScopes_ApiResources_ApiResourceId` FOREIGN KEY (`ApiResourceId`) REFERENCES `apiresources` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of apiscopes
-- ----------------------------
INSERT INTO `apiscopes` VALUES ('1', 'api1', 'API Application', null, '0', '0', '1', '1');

-- ----------------------------
-- Table structure for apisecrets
-- ----------------------------
DROP TABLE IF EXISTS `apisecrets`;
CREATE TABLE `apisecrets` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Value` varchar(4000) CHARACTER SET utf8mb4 NOT NULL,
  `Expiration` datetime(6) DEFAULT NULL,
  `Type` varchar(250) CHARACTER SET utf8mb4 NOT NULL,
  `Created` datetime(6) NOT NULL,
  `ApiResourceId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_ApiSecrets_ApiResourceId` (`ApiResourceId`),
  CONSTRAINT `FK_ApiSecrets_ApiResources_ApiResourceId` FOREIGN KEY (`ApiResourceId`) REFERENCES `apiresources` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of apisecrets
-- ----------------------------

-- ----------------------------
-- Table structure for aspnetroleclaims
-- ----------------------------
DROP TABLE IF EXISTS `aspnetroleclaims`;
CREATE TABLE `aspnetroleclaims` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `RoleId` int(11) NOT NULL,
  `ClaimType` longtext CHARACTER SET utf8mb4,
  `ClaimValue` longtext CHARACTER SET utf8mb4,
  PRIMARY KEY (`Id`),
  KEY `IX_AspNetRoleClaims_RoleId` (`RoleId`),
  CONSTRAINT `FK_AspNetRoleClaims_AspNetRoles_RoleId` FOREIGN KEY (`RoleId`) REFERENCES `aspnetroles` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aspnetroleclaims
-- ----------------------------

-- ----------------------------
-- Table structure for aspnetroles
-- ----------------------------
DROP TABLE IF EXISTS `aspnetroles`;
CREATE TABLE `aspnetroles` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(256) CHARACTER SET utf8mb4 DEFAULT NULL,
  `NormalizedName` varchar(256) CHARACTER SET utf8mb4 DEFAULT NULL,
  `ConcurrencyStamp` longtext CHARACTER SET utf8mb4,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `RoleNameIndex` (`NormalizedName`(255))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aspnetroles
-- ----------------------------
INSERT INTO `aspnetroles` VALUES ('1', 'Administrators', 'ADMINISTRATORS', '670915d9-9e55-47a9-a3e9-c90a0fd2de4f');

-- ----------------------------
-- Table structure for aspnetuserclaims
-- ----------------------------
DROP TABLE IF EXISTS `aspnetuserclaims`;
CREATE TABLE `aspnetuserclaims` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `ClaimType` longtext CHARACTER SET utf8mb4,
  `ClaimValue` longtext CHARACTER SET utf8mb4,
  PRIMARY KEY (`Id`),
  KEY `IX_AspNetUserClaims_UserId` (`UserId`),
  CONSTRAINT `FK_AspNetUserClaims_AspNetUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `aspnetusers` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aspnetuserclaims
-- ----------------------------

-- ----------------------------
-- Table structure for aspnetuserlogins
-- ----------------------------
DROP TABLE IF EXISTS `aspnetuserlogins`;
CREATE TABLE `aspnetuserlogins` (
  `LoginProvider` varchar(450) CHARACTER SET utf8mb4 NOT NULL,
  `ProviderKey` varchar(450) CHARACTER SET utf8mb4 NOT NULL,
  `ProviderDisplayName` longtext CHARACTER SET utf8mb4,
  `UserId` int(11) NOT NULL,
  PRIMARY KEY (`LoginProvider`(255),`ProviderKey`(255)),
  KEY `IX_AspNetUserLogins_UserId` (`UserId`),
  CONSTRAINT `FK_AspNetUserLogins_AspNetUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `aspnetusers` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aspnetuserlogins
-- ----------------------------

-- ----------------------------
-- Table structure for aspnetuserroles
-- ----------------------------
DROP TABLE IF EXISTS `aspnetuserroles`;
CREATE TABLE `aspnetuserroles` (
  `UserId` int(11) NOT NULL,
  `RoleId` int(11) NOT NULL,
  PRIMARY KEY (`UserId`,`RoleId`),
  KEY `IX_AspNetUserRoles_RoleId` (`RoleId`),
  CONSTRAINT `FK_AspNetUserRoles_AspNetRoles_RoleId` FOREIGN KEY (`RoleId`) REFERENCES `aspnetroles` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_AspNetUserRoles_AspNetUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `aspnetusers` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aspnetuserroles
-- ----------------------------
INSERT INTO `aspnetuserroles` VALUES ('1', '1');

-- ----------------------------
-- Table structure for aspnetusers
-- ----------------------------
DROP TABLE IF EXISTS `aspnetusers`;
CREATE TABLE `aspnetusers` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(256) CHARACTER SET utf8mb4 DEFAULT NULL,
  `NormalizedUserName` varchar(256) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Email` varchar(256) CHARACTER SET utf8mb4 DEFAULT NULL,
  `NormalizedEmail` varchar(256) CHARACTER SET utf8mb4 DEFAULT NULL,
  `EmailConfirmed` tinyint(1) NOT NULL,
  `PasswordHash` longtext CHARACTER SET utf8mb4,
  `SecurityStamp` longtext CHARACTER SET utf8mb4,
  `ConcurrencyStamp` longtext CHARACTER SET utf8mb4,
  `PhoneNumber` longtext CHARACTER SET utf8mb4,
  `PhoneNumberConfirmed` tinyint(1) NOT NULL,
  `TwoFactorEnabled` tinyint(1) NOT NULL,
  `LockoutEnd` datetime(6) DEFAULT NULL,
  `LockoutEnabled` tinyint(1) NOT NULL,
  `AccessFailedCount` int(11) NOT NULL,
  `Avatar` longtext CHARACTER SET utf8mb4,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `UserNameIndex` (`NormalizedUserName`(255)),
  KEY `EmailIndex` (`NormalizedEmail`(255))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aspnetusers
-- ----------------------------
INSERT INTO `aspnetusers` VALUES ('1', 'Administrator', 'ADMINISTRATOR', 'lxd_8023@qq.com', 'LXD_8023@QQ.COM', '0', 'AQAAAAEAACcQAAAAEG7ZcIOpzmbA5Z4COOokXfuGwkVsAcsB/PiYiDsmgDOcYDM1rvAyGbt0jQSsnUwQVA==', 'B3BXOLL6GHKJKTZMJ3HL7K2D3B2FT4PT', '1462e8a5-022a-469e-b865-032d782af197', null, '0', '0', null, '1', '0', 'https://baike.baidu.com/pic/%E5%BE%AE%E8%BD%AF/124767/0/2f738bd4b31c870124df02182e7f9e2f0608ffe7?fr=lemma&ct=single#aid=0&pic=2f738bd4b31c870124df02182e7f9e2f0608ffe7');

-- ----------------------------
-- Table structure for aspnetusertokens
-- ----------------------------
DROP TABLE IF EXISTS `aspnetusertokens`;
CREATE TABLE `aspnetusertokens` (
  `UserId` int(11) NOT NULL,
  `LoginProvider` varchar(450) CHARACTER SET utf8mb4 NOT NULL,
  `Name` varchar(450) CHARACTER SET utf8mb4 NOT NULL,
  `Value` longtext CHARACTER SET utf8mb4,
  PRIMARY KEY (`UserId`,`LoginProvider`(255),`Name`(255)),
  CONSTRAINT `FK_AspNetUserTokens_AspNetUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `aspnetusers` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aspnetusertokens
-- ----------------------------

-- ----------------------------
-- Table structure for clientclaims
-- ----------------------------
DROP TABLE IF EXISTS `clientclaims`;
CREATE TABLE `clientclaims` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(250) CHARACTER SET utf8mb4 NOT NULL,
  `Value` varchar(250) CHARACTER SET utf8mb4 NOT NULL,
  `ClientId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_ClientClaims_ClientId` (`ClientId`),
  CONSTRAINT `FK_ClientClaims_Clients_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `clients` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of clientclaims
-- ----------------------------

-- ----------------------------
-- Table structure for clientcorsorigins
-- ----------------------------
DROP TABLE IF EXISTS `clientcorsorigins`;
CREATE TABLE `clientcorsorigins` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Origin` varchar(150) CHARACTER SET utf8mb4 NOT NULL,
  `ClientId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_ClientCorsOrigins_ClientId` (`ClientId`),
  CONSTRAINT `FK_ClientCorsOrigins_Clients_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `clients` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of clientcorsorigins
-- ----------------------------

-- ----------------------------
-- Table structure for clientgranttypes
-- ----------------------------
DROP TABLE IF EXISTS `clientgranttypes`;
CREATE TABLE `clientgranttypes` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `GrantType` varchar(250) CHARACTER SET utf8mb4 NOT NULL,
  `ClientId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_ClientGrantTypes_ClientId` (`ClientId`),
  CONSTRAINT `FK_ClientGrantTypes_Clients_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `clients` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of clientgranttypes
-- ----------------------------
INSERT INTO `clientgranttypes` VALUES ('1', 'hybrid', '1');
INSERT INTO `clientgranttypes` VALUES ('2', 'client_credentials', '1');

-- ----------------------------
-- Table structure for clientidprestrictions
-- ----------------------------
DROP TABLE IF EXISTS `clientidprestrictions`;
CREATE TABLE `clientidprestrictions` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Provider` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `ClientId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_ClientIdPRestrictions_ClientId` (`ClientId`),
  CONSTRAINT `FK_ClientIdPRestrictions_Clients_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `clients` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of clientidprestrictions
-- ----------------------------

-- ----------------------------
-- Table structure for clientpostlogoutredirecturis
-- ----------------------------
DROP TABLE IF EXISTS `clientpostlogoutredirecturis`;
CREATE TABLE `clientpostlogoutredirecturis` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PostLogoutRedirectUri` varchar(2000) CHARACTER SET utf8mb4 NOT NULL,
  `ClientId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_ClientPostLogoutRedirectUris_ClientId` (`ClientId`),
  CONSTRAINT `FK_ClientPostLogoutRedirectUris_Clients_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `clients` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of clientpostlogoutredirecturis
-- ----------------------------
INSERT INTO `clientpostlogoutredirecturis` VALUES ('1', 'http://localhost:5001/signout-callback-oidc', '1');

-- ----------------------------
-- Table structure for clientproperties
-- ----------------------------
DROP TABLE IF EXISTS `clientproperties`;
CREATE TABLE `clientproperties` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Key` varchar(250) CHARACTER SET utf8mb4 NOT NULL,
  `Value` varchar(2000) CHARACTER SET utf8mb4 NOT NULL,
  `ClientId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_ClientProperties_ClientId` (`ClientId`),
  CONSTRAINT `FK_ClientProperties_Clients_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `clients` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of clientproperties
-- ----------------------------

-- ----------------------------
-- Table structure for clientredirecturis
-- ----------------------------
DROP TABLE IF EXISTS `clientredirecturis`;
CREATE TABLE `clientredirecturis` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `RedirectUri` varchar(2000) CHARACTER SET utf8mb4 NOT NULL,
  `ClientId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_ClientRedirectUris_ClientId` (`ClientId`),
  CONSTRAINT `FK_ClientRedirectUris_Clients_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `clients` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of clientredirecturis
-- ----------------------------
INSERT INTO `clientredirecturis` VALUES ('1', 'http://localhost:5001/signin-oidc', '1');

-- ----------------------------
-- Table structure for clients
-- ----------------------------
DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Enabled` tinyint(1) NOT NULL,
  `ClientId` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `ProtocolType` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `RequireClientSecret` tinyint(1) NOT NULL,
  `ClientName` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Description` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `ClientUri` varchar(2000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `LogoUri` varchar(2000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `RequireConsent` tinyint(1) NOT NULL,
  `AllowRememberConsent` tinyint(1) NOT NULL,
  `AlwaysIncludeUserClaimsInIdToken` tinyint(1) NOT NULL,
  `RequirePkce` tinyint(1) NOT NULL,
  `AllowPlainTextPkce` tinyint(1) NOT NULL,
  `AllowAccessTokensViaBrowser` tinyint(1) NOT NULL,
  `FrontChannelLogoutUri` varchar(2000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `FrontChannelLogoutSessionRequired` tinyint(1) NOT NULL,
  `BackChannelLogoutUri` varchar(2000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `BackChannelLogoutSessionRequired` tinyint(1) NOT NULL,
  `AllowOfflineAccess` tinyint(1) NOT NULL,
  `IdentityTokenLifetime` int(11) NOT NULL,
  `AccessTokenLifetime` int(11) NOT NULL,
  `AuthorizationCodeLifetime` int(11) NOT NULL,
  `ConsentLifetime` int(11) DEFAULT NULL,
  `AbsoluteRefreshTokenLifetime` int(11) NOT NULL,
  `SlidingRefreshTokenLifetime` int(11) NOT NULL,
  `RefreshTokenUsage` int(11) NOT NULL,
  `UpdateAccessTokenClaimsOnRefresh` tinyint(1) NOT NULL,
  `RefreshTokenExpiration` int(11) NOT NULL,
  `AccessTokenType` int(11) NOT NULL,
  `EnableLocalLogin` tinyint(1) NOT NULL,
  `IncludeJwtId` tinyint(1) NOT NULL,
  `AlwaysSendClientClaims` tinyint(1) NOT NULL,
  `ClientClaimsPrefix` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `PairWiseSubjectSalt` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Created` datetime(6) NOT NULL,
  `Updated` datetime(6) DEFAULT NULL,
  `LastAccessed` datetime(6) DEFAULT NULL,
  `UserSsoLifetime` int(11) DEFAULT NULL,
  `UserCodeType` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `DeviceCodeLifetime` int(11) NOT NULL,
  `NonEditable` tinyint(1) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `IX_Clients_ClientId` (`ClientId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of clients
-- ----------------------------
INSERT INTO `clients` VALUES ('1', '1', 'mvc', 'oidc', '1', 'MVC Client', null, 'http://localhost:5001', 'https://img-prod-cms-rt-microsoft-com.akamaized.net/cms/api/am/imageFileData/RE1Mu3b?ver=5c31', '1', '1', '1', '0', '0', '0', null, '1', null, '1', '1', '300', '3600', '300', null, '2592000', '1296000', '1', '0', '1', '0', '1', '0', '0', 'client_', null, '2019-07-31 07:28:53.101707', null, null, null, null, '300', '0');

-- ----------------------------
-- Table structure for clientscopes
-- ----------------------------
DROP TABLE IF EXISTS `clientscopes`;
CREATE TABLE `clientscopes` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Scope` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `ClientId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_ClientScopes_ClientId` (`ClientId`),
  CONSTRAINT `FK_ClientScopes_Clients_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `clients` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of clientscopes
-- ----------------------------
INSERT INTO `clientscopes` VALUES ('1', 'openid', '1');
INSERT INTO `clientscopes` VALUES ('2', 'profile', '1');
INSERT INTO `clientscopes` VALUES ('3', 'email', '1');
INSERT INTO `clientscopes` VALUES ('4', 'offline_access', '1');
INSERT INTO `clientscopes` VALUES ('5', 'api1', '1');

-- ----------------------------
-- Table structure for clientsecrets
-- ----------------------------
DROP TABLE IF EXISTS `clientsecrets`;
CREATE TABLE `clientsecrets` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(2000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Value` varchar(4000) CHARACTER SET utf8mb4 NOT NULL,
  `Expiration` datetime(6) DEFAULT NULL,
  `Type` varchar(250) CHARACTER SET utf8mb4 NOT NULL,
  `Created` datetime(6) NOT NULL,
  `ClientId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_ClientSecrets_ClientId` (`ClientId`),
  CONSTRAINT `FK_ClientSecrets_Clients_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `clients` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of clientsecrets
-- ----------------------------
INSERT INTO `clientsecrets` VALUES ('1', null, 'K7gNU3sdo+OL0wNhqoVWhr3g6s1xYv72ol/pe/Unols=', null, 'SharedSecret', '2019-07-31 07:28:53.102214', '1');

-- ----------------------------
-- Table structure for devicecodes
-- ----------------------------
DROP TABLE IF EXISTS `devicecodes`;
CREATE TABLE `devicecodes` (
  `UserCode` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `DeviceCode` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `SubjectId` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `ClientId` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `Expiration` datetime(6) NOT NULL,
  `Data` longtext CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`UserCode`),
  UNIQUE KEY `IX_DeviceCodes_DeviceCode` (`DeviceCode`),
  KEY `IX_DeviceCodes_Expiration` (`Expiration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of devicecodes
-- ----------------------------

-- ----------------------------
-- Table structure for identityclaims
-- ----------------------------
DROP TABLE IF EXISTS `identityclaims`;
CREATE TABLE `identityclaims` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `IdentityResourceId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_IdentityClaims_IdentityResourceId` (`IdentityResourceId`),
  CONSTRAINT `FK_IdentityClaims_IdentityResources_IdentityResourceId` FOREIGN KEY (`IdentityResourceId`) REFERENCES `identityresources` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of identityclaims
-- ----------------------------
INSERT INTO `identityclaims` VALUES ('1', 'sub', '1');
INSERT INTO `identityclaims` VALUES ('2', 'updated_at', '2');
INSERT INTO `identityclaims` VALUES ('3', 'locale', '2');
INSERT INTO `identityclaims` VALUES ('4', 'zoneinfo', '2');
INSERT INTO `identityclaims` VALUES ('5', 'birthdate', '2');
INSERT INTO `identityclaims` VALUES ('6', 'gender', '2');
INSERT INTO `identityclaims` VALUES ('7', 'website', '2');
INSERT INTO `identityclaims` VALUES ('8', 'email', '3');
INSERT INTO `identityclaims` VALUES ('9', 'picture', '2');
INSERT INTO `identityclaims` VALUES ('10', 'preferred_username', '2');
INSERT INTO `identityclaims` VALUES ('11', 'nickname', '2');
INSERT INTO `identityclaims` VALUES ('12', 'middle_name', '2');
INSERT INTO `identityclaims` VALUES ('13', 'given_name', '2');
INSERT INTO `identityclaims` VALUES ('14', 'family_name', '2');
INSERT INTO `identityclaims` VALUES ('15', 'name', '2');
INSERT INTO `identityclaims` VALUES ('16', 'profile', '2');
INSERT INTO `identityclaims` VALUES ('17', 'email_verified', '3');

-- ----------------------------
-- Table structure for identityproperties
-- ----------------------------
DROP TABLE IF EXISTS `identityproperties`;
CREATE TABLE `identityproperties` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Key` varchar(250) CHARACTER SET utf8mb4 NOT NULL,
  `Value` varchar(2000) CHARACTER SET utf8mb4 NOT NULL,
  `IdentityResourceId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_IdentityProperties_IdentityResourceId` (`IdentityResourceId`),
  CONSTRAINT `FK_IdentityProperties_IdentityResources_IdentityResourceId` FOREIGN KEY (`IdentityResourceId`) REFERENCES `identityresources` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of identityproperties
-- ----------------------------

-- ----------------------------
-- Table structure for identityresources
-- ----------------------------
DROP TABLE IF EXISTS `identityresources`;
CREATE TABLE `identityresources` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Enabled` tinyint(1) NOT NULL,
  `Name` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `DisplayName` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Description` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Required` tinyint(1) NOT NULL,
  `Emphasize` tinyint(1) NOT NULL,
  `ShowInDiscoveryDocument` tinyint(1) NOT NULL,
  `Created` datetime(6) NOT NULL,
  `Updated` datetime(6) DEFAULT NULL,
  `NonEditable` tinyint(1) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `IX_IdentityResources_Name` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of identityresources
-- ----------------------------
INSERT INTO `identityresources` VALUES ('1', '1', 'openid', 'Your user identifier', null, '1', '0', '1', '2019-07-31 07:28:53.349536', null, '0');
INSERT INTO `identityresources` VALUES ('2', '1', 'profile', 'User profile', 'Your user profile information (first name, last name, etc.)', '0', '1', '1', '2019-07-31 07:28:53.370111', null, '0');
INSERT INTO `identityresources` VALUES ('3', '1', 'email', 'Your email address', null, '0', '1', '1', '2019-07-31 07:28:53.378095', null, '0');

-- ----------------------------
-- Table structure for persistedgrants
-- ----------------------------
DROP TABLE IF EXISTS `persistedgrants`;
CREATE TABLE `persistedgrants` (
  `Key` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `Type` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `SubjectId` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `ClientId` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `Expiration` datetime(6) DEFAULT NULL,
  `Data` longtext CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`Key`),
  KEY `IX_PersistedGrants_SubjectId_ClientId_Type_Expiration` (`SubjectId`,`ClientId`,`Type`,`Expiration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of persistedgrants
-- ----------------------------
INSERT INTO `persistedgrants` VALUES ('/ES5SMsHFRnHM64+nDRmQgL7QBG+S35IOijUunTZl4k=', 'authorization_code', '1', 'mvc', '2019-07-31 07:37:09.000000', '2019-07-31 07:42:09.000000', '{\"CreationTime\":\"2019-07-31T07:37:09Z\",\"Lifetime\":300,\"ClientId\":\"mvc\",\"Subject\":{\"AuthenticationType\":\"Identity.Application\",\"Claims\":[{\"Type\":\"sub\",\"Value\":\"1\",\"ValueType\":\"http://www.w3.org/2001/XMLSchema#string\"},{\"Type\":\"AspNet.Identity.SecurityStamp\",\"Value\":\"B3BXOLL6GHKJKTZMJ3HL7K2D3B2FT4PT\",\"ValueType\":\"http://www.w3.org/2001/XMLSchema#string\"},{\"Type\":\"role\",\"Value\":\"Administrators\",\"ValueType\":\"http://www.w3.org/2001/XMLSchema#string\"},{\"Type\":\"preferred_username\",\"Value\":\"Administrator\",\"ValueType\":\"http://www.w3.org/2001/XMLSchema#string\"},{\"Type\":\"name\",\"Value\":\"Administrator\",\"ValueType\":\"http://www.w3.org/2001/XMLSchema#string\"},{\"Type\":\"email\",\"Value\":\"lxd_8023@qq.com\",\"ValueType\":\"http://www.w3.org/2001/XMLSchema#string\"},{\"Type\":\"email_verified\",\"Value\":\"false\",\"ValueType\":\"http://www.w3.org/2001/XMLSchema#boolean\"},{\"Type\":\"idp\",\"Value\":\"local\",\"ValueType\":\"http://www.w3.org/2001/XMLSchema#string\"},{\"Type\":\"amr\",\"Value\":\"pwd\",\"ValueType\":\"http://www.w3.org/2001/XMLSchema#string\"},{\"Type\":\"auth_time\",\"Value\":\"1564558585\",\"ValueType\":\"http://www.w3.org/2001/XMLSchema#integer\"}]},\"IsOpenId\":true,\"RequestedScopes\":[\"openid\",\"profile\"],\"RedirectUri\":\"http://localhost:5001/signin-oidc\",\"Nonce\":\"637001553696113129.ZjQ2MjRlMDctM2Y0ZS00YjQ4LTlkZTctY2M2MjcxODI1MjI1Y2RkZWJjNjEtYjRjMS00ZjI2LWFiMmItZmExNmQ1MTg3NzJk\",\"WasConsentShown\":true,\"SessionId\":\"e7e38be1facddcbbe3effcf55d25ecd2\",\"CodeChallenge\":\"\",\"CodeChallengeMethod\":null,\"Properties\":{}}');
INSERT INTO `persistedgrants` VALUES ('I/VFvztpgLvo3NzKmjiGBJb2JDPKZImDQHYs8ONNctk=', 'authorization_code', '1', 'mvc', '2019-07-31 07:36:30.000000', '2019-07-31 07:41:30.000000', '{\"CreationTime\":\"2019-07-31T07:36:30Z\",\"Lifetime\":300,\"ClientId\":\"mvc\",\"Subject\":{\"AuthenticationType\":\"Identity.Application\",\"Claims\":[{\"Type\":\"sub\",\"Value\":\"1\",\"ValueType\":\"http://www.w3.org/2001/XMLSchema#string\"},{\"Type\":\"AspNet.Identity.SecurityStamp\",\"Value\":\"B3BXOLL6GHKJKTZMJ3HL7K2D3B2FT4PT\",\"ValueType\":\"http://www.w3.org/2001/XMLSchema#string\"},{\"Type\":\"role\",\"Value\":\"Administrators\",\"ValueType\":\"http://www.w3.org/2001/XMLSchema#string\"},{\"Type\":\"preferred_username\",\"Value\":\"Administrator\",\"ValueType\":\"http://www.w3.org/2001/XMLSchema#string\"},{\"Type\":\"name\",\"Value\":\"Administrator\",\"ValueType\":\"http://www.w3.org/2001/XMLSchema#string\"},{\"Type\":\"email\",\"Value\":\"lxd_8023@qq.com\",\"ValueType\":\"http://www.w3.org/2001/XMLSchema#string\"},{\"Type\":\"email_verified\",\"Value\":\"false\",\"ValueType\":\"http://www.w3.org/2001/XMLSchema#boolean\"},{\"Type\":\"idp\",\"Value\":\"local\",\"ValueType\":\"http://www.w3.org/2001/XMLSchema#string\"},{\"Type\":\"amr\",\"Value\":\"pwd\",\"ValueType\":\"http://www.w3.org/2001/XMLSchema#string\"},{\"Type\":\"auth_time\",\"Value\":\"1564558585\",\"ValueType\":\"http://www.w3.org/2001/XMLSchema#integer\"}]},\"IsOpenId\":true,\"RequestedScopes\":[\"openid\",\"profile\"],\"RedirectUri\":\"http://localhost:5001/signin-oidc\",\"Nonce\":\"637001553696113129.ZjQ2MjRlMDctM2Y0ZS00YjQ4LTlkZTctY2M2MjcxODI1MjI1Y2RkZWJjNjEtYjRjMS00ZjI2LWFiMmItZmExNmQ1MTg3NzJk\",\"WasConsentShown\":true,\"SessionId\":\"e7e38be1facddcbbe3effcf55d25ecd2\",\"CodeChallenge\":\"\",\"CodeChallengeMethod\":null,\"Properties\":{}}');
INSERT INTO `persistedgrants` VALUES ('wpoLTEdzjFGLw3nr0g0sE+zpRZAD/+/LeapdwXDttM4=', 'user_consent', '1', 'mvc', '2019-07-31 07:37:09.000000', null, '{\"SubjectId\":\"1\",\"ClientId\":\"mvc\",\"Scopes\":[\"openid\",\"profile\"],\"CreationTime\":\"2019-07-31T07:37:09Z\",\"Expiration\":null}');

-- ----------------------------
-- Table structure for __efmigrationshistory
-- ----------------------------
DROP TABLE IF EXISTS `__efmigrationshistory`;
CREATE TABLE `__efmigrationshistory` (
  `MigrationId` varchar(150) CHARACTER SET utf8mb4 NOT NULL,
  `ProductVersion` varchar(32) CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`MigrationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of __efmigrationshistory
-- ----------------------------
INSERT INTO `__efmigrationshistory` VALUES ('20190731072741_init', '2.2.6-servicing-10079');
INSERT INTO `__efmigrationshistory` VALUES ('20190731072747_init', '2.2.6-servicing-10079');
INSERT INTO `__efmigrationshistory` VALUES ('20190731072752_AppDbMigration', '2.2.6-servicing-10079');
